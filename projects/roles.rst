#####
Roles
#####

.. contents::
   :local:
   :backlinks: none

***************
Project manager
***************

While there are different scales of projects in LSF, in the broadest sense,
project managers (PMs) are responsible for planning, organizing, and directing
the completion of specific projects for LSF while ensuring these projects are
on time, on budget, and within scope.

Specifics on the tooling and processes can be found in
:doc:`project-management`.

****************
Project champion
****************

In order to ensure efficient and in-time resource allocation, each project
employs a project champion role that is responsible to:

* Document project progress and report back to the rest of the org
* Communicate clear priorities and resource needs to the rest of the org
* Timely identify blockers and seek resolutions
* Critically analyzing and ensuring best practices for project management in
  collaboration with the Project Manager

It is possible that Project Champion and Project Manager is the same person,
although it is envisioned as a separate individual for the larger projects of
the org.

*****
Peers
*****

Peers are the people that are participants in a project.


***************
Role assignment
***************

Whenever individually or in project-group level there is a need for role assignments the
following process is followed:

#. First a review of the current role assignments is carried out to ensure that it
   reflects the present and immediate past situation by the PM and the group.
#. If there is a need for new roles then the "Role Definition" (see below) process
   should be followed.
#. Then peers of the project select the roles they want to be assigned in and input
   there perceived effort for the foreseeable future. In some projects this might be 1-3
   months, in others it might be longer.
#. For roles that are not assigned to current peers, the marking "VACANT" should be used.
#. The "VACANT" should be advertised in lsf-core level.
#. If no peers are found to meet the "VACANT" role, the project manager creates a new job
   description to meet the need and brings it up to the HR team (process TBD).

.. note::
   When not clear ownership of a step is defined, then "Project Manager" is assumed as
   the owner of that step/action.

.. warning::
   People should ensure that they are not over-assigned in various projects.
   Check "People" Sheet when in doubt.


***************
Role definition
***************

When there is a need for a new role definition, based on the project management process,
the following steps are followed:

#. Check from available role definitions in "Roles" Sheet
#. If there is no exact match with the new role, create a new role in "Roles" Sheet with
   detailed description
#. On the project "Sheet" create a new row and add the newly defined role
#. Ensure the project peers are aware of the addition

.. note::
   Roles could be refined by individuals at any moment.

   Don't be afraid to create a new super-specific role describing your role in a project.

   If there is a need to merge, reach-out to the PMs of the projects using the roles.
