# Core contributor removal #

## Actions ##

### Human resources ###

- [ ] Open issue on HR service desk to [remove your PGP key ID](https://gitlab.com/librespacefoundation/lsf-hr-servicedesk/-/issues/new?issuable_template=Payslip%20-%20Update%20PGP%20key&issue[title]=Update%20payslip%20PGP%20key) from the payslip database (paid only)

### Email ###

- [ ] Open issue on IT operations support to [turn off your LSF email account](https://gitlab.com/librespacefoundation/ops/support/-/issues/new?issuable_template=Email%20-%20Disable%20account&issue[title]=Disable%20email%20account).

### GitLab ###

- [ ] Remove yourself as an assignee from lsf-core issues
- [ ] Open issue on IT operations support to [be removed as a developer](https://gitlab.com/librespacefoundation/ops/support/-/issues/new?issuable_template=GitLab%20-%20Modify%20project%20developers&issue[title]=Modify%20GitLab%20project%20developers) from GitLab projects:
  - `librespacefoundation/lsf-hr-leaves`, if paid-contributor
  - `librespacefoundation/lsf-core`
  - `librespacefoundation/lsf-org`

### Matrix ###

- [ ] Leave `#lsf:matrix.org` (optional)
- [ ] Open issue on IT operations support to [be removed to `#lsf-core:matrix.org` channel](https://gitlab.com/librespacefoundation/ops/support/-/issues/new?issuable_template=Matrix%20-%20Remove%20user%20from%20invite-only%20channel&issue[title]=Remove%20user%20from%20matrix%20invite-only%20channel)

### Community ###

- [ ] Open issue on HR service desk to [remove a member of `lsf-core` community group](https://gitlab.com/librespacefoundation/lsf-hr-servicedesk/-/issues/new)
- [ ] Open issue on [Libre Space Communications service desk](https://gitlab.com/librespacefoundation/lsf-comms-servicedesk) for removing you from the contributors list on `About Us` page of [libre.space](https://libre.space) website

### Cloud ###

- Open issues on IT operations support to:
  - [ ] [Disable a Cloud user](https://gitlab.com/librespacefoundation/ops/support/-/issues/new?issuable_template=Cloud%20-%20Modify%20user&issue[title]=Disable%20Cloud%20user).

### Travel ###

- [ ] Remove individual airbnb account from airbnb work account

### Procurement ###

- [ ] [Disable your Amazon Business account](https://www.amazon.de/) using your LSF email
- [ ] Update [Procurement page](https://docs.libre.space/en/latest/procurement.html) to remove access if needed

### Meetings ###

- [ ] Ask the meeting creators to be removed from [regular LSF meetings](https://docs.libre.space/en/stable/collaboration/meetings.html#regular-lsf-meetings)


- [ ] Say good bye and farewell to lsf-core channel

/confidential
